'use strict';
// (function () {
try {

    var AppMovieModule = angular.module('AppMovieModule', ['ngRoute']);


    AppMovieModule.config(function ($routeProvider) {


        $routeProvider.when('/featured', {
            templateUrl: 'partials/featured.html'
        });

        $routeProvider.otherwise({
            templateUrl: 'partials/search.html'
        });
    })
    AppMovieModule.constant("API_ENDPOINT", "http://www.omdbapi.com/?i=tt3896198&apikey=f50c58b4");


    AppMovieModule.controller('AppMainController', function ($scope, $http, API_ENDPOINT) {

        /*$scope model object our data*/
        $scope.Movies = {};
        /*app settings*/
        $scope.Movies.settings = {
            appName: 'Sogeti',
            themeColor: '#0000FF',
            textColor: '#00000'
        };

        $scope.Movies.changeMoviePlot = function () {
            if ($scope.Movies.moviePlot === 'short') {
                $scope.Movies.moviePlot = 'full';
            }
            else
                $scope.Movies.moviePlot = 'short';

            $scope.Movies.searchMovie($scope.Movies.movieTitle);
        }
        $scope.Movies.request = {
            error: false,
            status: false,
            requestStatus: false,
            startSearch: false,
            message: ''
        }
        $scope.Movies.movieTitle = '';
        $scope.Movies.moviePlot = 'full';
        $scope.Movies.movieDetails = null;

        $scope.Movies.searchMovie = function (movie) {
            $scope.Movies.request.startSearch = true;
            if (movie === '') {
                return;
            }
            var url = API_ENDPOINT + "&t=" + movie + "&plot=" + $scope.Movies.moviePlot;
            try {
                $scope.Movies.request = {
                    error: false,
                    status: false,
                    requestStatus: true,
                    message: 'Request in progress'
                }
                $http({
                    method: 'GET',
                    url: url
                }).then(function successCallback(response) {
                    $scope.Movies.request.requestStatus = false;
                    if (response.status === 200) {
                        var res = response.data;
                        if (res.Response === "False") {

                            $scope.Movies.request = {
                                status: true,
                                message: res.Error
                            }
                            $scope.Movies.movieDetails = null;
                            return;
                        }
                        $scope.Movies.movieDetails = response.data;

                    } else {
                        $scope.Movies.request = {
                            error: true,
                            message: 'An error occurred,we could not retrieve the movie'
                        }
                    }

                }, function errorCallback(response) {
                    $scope.Movies.request = {
                        error: true,
                        status: false,
                        requestStatus: false,
                        message: 'Server error occured,please try again'
                    }
                    console.log(response)

                });


            } catch (ex) {
                console.error(ex);
            }

        };


        $scope.Movies.splitString = function (stringToBeSplit) {
            return stringToBeSplit.split(",");

        }

        $scope.Movies.predefinedMovies = [
            {
                "Title": "Tenacious D in The Pick of Destiny",
                "Year": "2006",
                "Rated": "R",
                "Released": "22 Nov 2006",
                "Runtime": "93 min",
                "Genre": "Adventure, Comedy, Music",
                "Director": "Liam Lynch",
                "Writer": "Jack Black, Kyle Gass, Liam Lynch",
                "Actors": "Jack Black, Kyle Gass, JR Reed, Ronnie James Dio",
                "Plot": "To become the greatest band of all time, two slacker, wannabe-rockers set out on a quest to steal a legendary guitar pick that gives its holders incredible guitar skills, from a maximum security Rock and Roll museum.",
                "Language": "English",
                "Country": "USA, Germany",
                "Awards": "2 nominations.",
                "Poster": "https://ia.media-imdb.com/images/M/MV5BMTUyMDA3OTc4MV5BMl5BanBnXkFtZTcwNzE5NjkzMQ@@._V1_SX300.jpg",
                "Ratings": [{"Source": "Internet Movie Database", "Value": "6.8/10"}, {
                    "Source": "Rotten Tomatoes",
                    "Value": "54%"
                }, {"Source": "Metacritic", "Value": "55/100"}],
                "Metascore": "55",
                "imdbRating": "6.8",
                "imdbVotes": "92,891",
                "imdbID": "tt0365830",
                "Type": "movie",
                "DVD": "27 Feb 2007",
                "BoxOffice": "$8,199,999",
                "Production": "New Line Cinema",
                "Website": "http://tenaciousdmovie.com/",
                "Response": "True"
            },
            {
                "Title": "Rampage",
                "Year": "2018",
                "Rated": "PG-13",
                "Released": "13 Apr 2018",
                "Runtime": "107 min",
                "Genre": "Action, Adventure, Sci-Fi",
                "Director": "Brad Peyton",
                "Writer": "Ryan Engle (screenplay by), Carlton Cuse (screenplay by), Ryan J. Condal (screenplay by), Adam Sztykiel (screenplay by), Ryan Engle (story by)",
                "Actors": "Dwayne Johnson, Naomie Harris, Malin Akerman, Jeffrey Dean Morgan",
                "Plot": "When three different animals become infected with a dangerous pathogen, a primatologist and a geneticist team up to stop them from destroying Chicago.",
                "Language": "English, Spanish, American Sign Language",
                "Country": "USA",
                "Awards": "N/A",
                "Poster": "https://m.media-amazon.com/images/M/MV5BNDA1NjA3ODU3OV5BMl5BanBnXkFtZTgwOTg3MTIwNTM@._V1_SX300.jpg",
                "Ratings": [{"Source": "Internet Movie Database", "Value": "6.2/10"}, {
                    "Source": "Rotten Tomatoes",
                    "Value": "52%"
                }, {"Source": "Metacritic", "Value": "45/100"}],
                "Metascore": "45",
                "imdbRating": "6.2",
                "imdbVotes": "62,028",
                "imdbID": "tt2231461",
                "Type": "movie",
                "DVD": "26 Jun 2018",
                "BoxOffice": "N/A",
                "Production": "Warner Bros. Pictures",
                "Website": "http://www.rampagethemovie.com/home",
                "Response": "True"
            }

        ]


    })


} catch (ex) {
    console.log(ex)
}
// })